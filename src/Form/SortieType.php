<?php

namespace App\Form;

use App\Entity\Event;
use App\Entity\Place;
use App\Repository\PlaceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SortieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => false
            ])
            ->add('startingTime', DateType::class, [
                'html5' => true,
                'widget' => 'single_text'
            ])
            ->add('lastRegistrationDate', DateType::class, [
                'html5' => true,
                'widget' => 'single_text'
            ])
            ->add('duration', NumberType::class)
            ->add('nbMaxRegistration', NumberType::class)
            ->add('information', TextType::class, [
                'required' => false
            ])
            ->add('place', EntityType::class, [
                'class' => Place::class,
                'choice_label' => 'name',
                'query_builder' => function(PlaceRepository $placeRepository){
                    return $placeRepository->createQueryBuilder('l')->addOrderBy('l.name');
                },
            ])
            ->add('publish', SubmitType::class, [
                'label' => 'Publier'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'required' => false
        ]);
    }
}

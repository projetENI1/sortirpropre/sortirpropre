<?php

namespace App\Form;

use App\Entity\Filter;
use App\Entity\Place;
use App\Entity\Site;
use App\Repository\PlaceRepository;
use App\Repository\SiteRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Le nom de la sortie contient :',
                'required' => false
            ])
            ->add('startingDate', DateTimeType::class, [
                'label' => 'Entre',
                'required' => false,
                'html5' => true,
                'widget' => 'single_text'
            ])
            ->add('endDate', DateTimeType::class, [
                'label' => 'Et ',
                'required' => false,
                'html5' => true,
                'widget' => 'single_text'
            ])
            ->add('isOrganisateur', CheckboxType::class, [
                'label' => 'Sorties dont je suis l\'organisateur/trice',
                'required' => false
            ])
            ->add('isInscrit', CheckboxType::class, [
                'label' => 'Sorties auxquelles je suis inscrit/e',
                'required' => false
            ])
            ->add('isNotInscrit', CheckboxType::class, [
                'label' => 'Sorties auquelles je ne suis pas inscrit/e',
                'required' => false
            ])
            ->add('isPasse')
            ->add('site', EntityType::class, [
                'class' => Site::class,
                'required' => false,
                'choice_label' => 'name',
                'query_builder' => function(SiteRepository $siteRepository) {
                    return $siteRepository->createQueryBuilder('l')->addOrderBy('l.name');
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Filter::class,
        ]);
    }
}

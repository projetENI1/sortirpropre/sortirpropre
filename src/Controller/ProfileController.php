<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\EventRepository;
use App\Repository\SiteRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/profile', name: 'profile_')]
class ProfileController extends AbstractController
{
    #[Route('/edit/{id}', name: 'edit', requirements: ['id' => '\d+'])]
    public function edit(Request $request, UserRepository $userRepository, UserPasswordHasherInterface $userPasswordHasher,int $id = null): Response
    {

        $userConnected = $userRepository->findOneBy(['email'=> $this->getUser()->getUserIdentifier()]);
        $user = $userRepository->findOneByEmail($this->getUser()->getUserIdentifier());


        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->get('password')->getData()) {
                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                        $user,
                        $form->get('password')->getData()
                    )
                );
            }

            if ($form->isValid()) {
                $userRepository->save($user, true);
                $this->addFlash('success', 'Profile edited');


                return $this->redirectToRoute('profile_show', ['id' => $userConnected->getId()]);
            }
        }

        return $this->render('profile/edit.html.twig', [
            'editProfileForm' => $form->createView(),
            'user' => $user
        ]);
    }

    #[Route('/detail/{id}', name: 'show', requirements: ['id' => '\d+'])]
    public function show(User $user, UserRepository $userRepository): Response
    {

        $userCurrent = $this->getUser();
        $campus = $user->getCampus();

        return $this->render('profile/show.html.twig', [
            'user' => $user,
            'campus' => $campus,
            'userCurrent' => $userCurrent
        ]);
    }
}

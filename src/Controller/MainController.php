<?php

namespace App\Controller;

use App\Service\CheckEventService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/main', name: 'main_')]
class MainController extends AbstractController
{
    #[Route('', name: 'home')]
    public function index(CheckEventService $checkEventService): Response
    {
        $checkEventService->EventToArchive();
        return $this->render('main/home.html.twig');
    }

    #[Route('/about', name: 'about')]
    public function about(): Response
    {
        return $this->render('main/about.html.twig');
    }

    #[Route('/cgu', name: 'cgu')]
    public function cgu(): Response
    {
        return $this->render('main/cgu.html.twig');
    }
}

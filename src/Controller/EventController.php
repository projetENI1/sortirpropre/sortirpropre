<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Filter;
use App\Entity\State;
use App\Entity\User;
use App\Form\AnnulationType;
use App\Form\FilterType;
use App\Form\SortieType;
use App\Repository\CityRepository;
use App\Repository\EventRepository;
use App\Repository\FilterRepository;
use App\Repository\ParticipantRepository;
use App\Repository\PlaceRepository;
use App\Repository\SiteRepository;
use App\Repository\StateRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

#[Route('event', name: 'event_')]
class EventController extends AbstractController
{

    private Security $security;

    public function __construct(
        Security $security,
        private readonly StateRepository $stateRepository,
        private readonly CityRepository $cityRepository,
        private readonly EventRepository $eventRepository,
        private readonly SiteRepository $siteRepository,
        private readonly PlaceRepository $placeRepository,
        private readonly UserRepository $userRepository

    ){
        $this->security = $security;
    }

    #[Route('/', name: 'list')]
    public function list(Request $request, EventRepository $eventRepository): Response
    {
        $filter = new Filter();
        $filterForm = $this->createForm(FilterType::class, $filter);
        $user = $this->getUser();
        if($user != null) {
            $userConnected = $this->userRepository->findOneBy(['email'=> $user->getUserIdentifier()]);
        }


        $filterForm->handleRequest($request);

        $events = $this->eventRepository->findAll();

        if ($filterForm->isSubmitted() && $filterForm->isValid()) {
            $events = $eventRepository->filter($filter, $userConnected );
        }

        return $this->render('event/list.html.twig', [
            'events' => $events,
            'user' => $user,
            'filterForm' => $filterForm->createView()
        ]);
    }

    #[Route('/detail/{id}', name: 'show', requirements: ['id' => '\d+'])]
    public function show(Event $event): Response
    {
        $user = $this->getUser();
        return $this->render('event/show.html.twig', [
            'event' => $event,
            'user' => $user
        ]);
    }


    #[Route('/edit/{id}', name: 'edit', requirements: ['id' => '\d+'])]
    #[Route('/add', name: 'add')]
    public function addOrEdit(Request $request, int $id = null): Response
    {
        if ($id) {
            $event = $this->eventRepository->find($id);
        } else {
            $event = new Event();
        }

        $user = $this->getUser();

        $eventForm = $this->createForm(SortieType::class, $event);
        $site = $this->siteRepository->findAll();

        $eventForm->handleRequest($request);

        if ($eventForm->isSubmitted() && $eventForm->isValid()) {
            if($eventForm->get('publish')->isClicked()){
                $stateLabel ='Ouvert';
            } else {
                $stateLabel ='En Création';
            }
            $state = $this->stateRepository->findOneByLabel($stateLabel);

            // mise à jour de l'entité
            $event->setOrganisateur($user);
            $event->setCampus($this->userRepository->findOneBy(['email' => $user->getUserIdentifier()])->getCampus());
            $event->setState($state);

            // enregistrement des données
            $this->eventRepository->add($event, true);


            // feedback user
            $this->addFlash('success', 'event' . ($id ? ' edited' : ' added') . ' !');

            // redirection vers la page de détail
            return $this->redirectToRoute('event_show', ['id' => $event->getId()]);
        }

        if ($id) {
            return $this->render('event/edit.html.twig', [
                'eventForm' => $eventForm->createView()
            ]);
        } else {
            return $this->render('event/add.html.twig', [
                'eventForm' => $eventForm->createView()
            ]);
        }

    }

    #[Route('/inscription/{id}', name: 'inscription', requirements: ['id' => '\d+'])]
    public function inscription(int $id): Response
    {
        $user = $this->security->getUser();
        $event = $this->eventRepository->findOneById($id);
        $event->addLstUser($user);
        $this->eventRepository->add($event, true);

        return $this->redirectToRoute('event_list');
    }

    #[Route('/desinscription/{id}', name: 'desinscription', requirements: ['id' => '\d+'])]
    public function desinscription(int $id): Response
    {
        $user = $this->security->getUser();
        $event = $this->eventRepository->findOneById($id);
        $event->removeLstUser($user);
        $this->eventRepository->add($event, true);

        return $this->redirectToRoute('event_list');
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(Request $request,StateRepository $stateRepository, EventRepository $eventRepository, int $id): Response
    {
        $event = $eventRepository->findOneById($id);
        $user = $this->getUser();

        $deleteForm = $this->createForm(AnnulationType::class);
        $deleteForm->handleRequest($request);


        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            $motif = $deleteForm->get('motif')->getData();
            $event->setInformation('SORTIE ANNULEE - ' . $motif);
            $event->setState($stateRepository->findOneByLabel('Annulé'));
            $eventRepository->add($event, true);

            return $this->redirectToRoute('event_list');
        }

        return $this->render('event/annuler.html.twig', [
            'event' => $event,
            'user' => $user,
            'deleteForm' => $deleteForm->createView()
        ]);
    }


}
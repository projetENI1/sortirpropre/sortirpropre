<?php

namespace App\Service;

use App\Repository\EventRepository;
use App\Repository\StateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Service use for check events status or available actions
 * Class CheckEventService
 * @package App\Service
 */
class CheckEventService
{
    private $eventRepository;
    private $stateRepository;
    private $em;

    public function __construct(EventRepository $eventRepository, StateRepository $stateRepository, EntityManagerInterface $em)
    {
        $this->eventRepository = $eventRepository;
        $this->stateRepository = $stateRepository;
        $this->em = $em;
    }

    /**
     * Check if an event need to be archive
     * @throws Exception
     */
    public function EventToArchive(): void
    {
        $now = new \DateTime();
        // ajout d'un mois
        $dateCompare = $now->add(new \DateInterval('P1M'));
        $events = $this->eventRepository->findAll();
        foreach ($events as $event) {
            $dateEvent = ($event->getStartingTime())->add(new \DateInterval('P' . $event->getDuration() . 'D'));
            if ($event->getState() !== 7){
                if ( $dateCompare > $dateEvent){
                    $idStateArchive = $this->stateRepository->findOneByLabel('Archivé');
                    $event->setState($idStateArchive);

                    $this->em->persist($event);
                    $this->em->flush();
                }
            }
        }
    }
}
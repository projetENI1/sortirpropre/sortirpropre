<?php

namespace App\Entity;

use App\Repository\FilterRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FilterRepository::class)]
class Filter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $startingDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $endDate = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isOrganisateur = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isInscrit = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isNotInscrit = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isPasse = null;


    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Site $site = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartingDate(): ?\DateTimeInterface
    {
        return $this->startingDate;
    }

    public function setStartingDate(?\DateTimeInterface $startingDate): self
    {
        $this->startingDate = $startingDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function isIsOrganisateur(): ?bool
    {
        return $this->isOrganisateur;
    }

    public function setIsOrganisateur(?bool $isOrganisateur): self
    {
        $this->isOrganisateur = $isOrganisateur;

        return $this;
    }

    public function isIsInscrit(): ?bool
    {
        return $this->isInscrit;
    }

    public function setIsInscrit(?bool $isInscrit): self
    {
        $this->isInscrit = $isInscrit;

        return $this;
    }

    public function isIsNotInscrit(): ?bool
    {
        return $this->isNotInscrit;
    }

    public function setIsNotInscrit(?bool $isNotInscrit): self
    {
        $this->isNotInscrit = $isNotInscrit;

        return $this;
    }

    public function isIsPasse(): ?bool
    {
        return $this->isPasse;
    }

    public function setIsPasse(?bool $isPasse): self
    {
        $this->isPasse = $isPasse;

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }
}

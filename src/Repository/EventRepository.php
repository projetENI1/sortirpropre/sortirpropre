<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\Filter;
use App\Entity\State;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Event>
 *
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    private $stateRepository;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager )
    {
        parent::__construct($registry, Event::class);
        $this->stateRepository = $entityManager->getRepository(State::class);
    }


    public function add(Event $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Event $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    // peut-être dans le repo de events
    public function filter(Filter $filter, User $user) {



        $qb = $this->createQueryBuilder('s');

        // Filtre sur le campus
        if ($filter->getSite()) {
            $qb->andWhere('s.campus = :site')
                ->setParameter('site', $filter->getSite());
        }
        // Filtre nom
        if ($filter->getName()) {
            $qb->andWhere('s.name LIKE :name')
                ->setParameter('name', '%'.$filter->getName().'%');
        }
        // Filtre dates
        if ($filter->getStartingDate() && $filter->getEndDate()) {
            $qb->andWhere('s.startingTime BETWEEN :startingDate AND :endDate')
                ->setParameter('startingDate', $filter->getStartingDate())
                ->setParameter('endDate', $filter->getEndDate());
        }
        // Filtre organisateurs
        if ($filter->isIsOrganisateur()) {
            $qb->andWhere('s.organisateur = :idOrganisateur')
                ->setParameter('idOrganisateur', $user->getId());
        }
        // Filtre inscrit
        if ($filter->isIsInscrit() && !$filter->isIsNotInscrit()) {
            $qb->join('s.lstUser', 'p')
                ->andWhere(':userId IN (p.id)')
                ->setParameter('userId', $user->getId());
        }
        // Filtre event passé
        if ($filter->isIsPasse()) {
            $qb->andWhere('s.startingDate < :today')
                ->setParameter('today', new \DateTime());
        }

        return $qb->getQuery()->getResult();

    }

//    /**
//     * @return Event[] Returns an array of Event objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Event
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

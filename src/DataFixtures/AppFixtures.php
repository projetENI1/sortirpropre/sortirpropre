<?php



namespace App\DataFixtures;



use App\Entity\City;
use App\Entity\Event;
use App\Entity\Place;
use App\Entity\Site;
use App\Entity\State;
use App\Entity\User;
use DateInterval;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $generator = Factory::create('fr_FR');
        $this->addState($manager);
        $this->addSite($manager, $generator);
        $this->addCity($manager, $generator);
        $this->addPlace($manager, $generator);
        $this->addUser($manager, $generator);
        $this->addEvent($manager, $generator);
    }



    private UserPasswordEncoderInterface $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    public function addState(ObjectManager $manager) {
        $etatEnCreation = new State();
        $etatOuvert = new State();
        $etatCloture = new State();
        $etatEnCours = new State();
        $etatTermine = new State();
        $etatAnnule = new State();
        $etatArchive = new State();

        $etatEnCreation->setLabel("En Création");
        $etatOuvert->setLabel("Ouvert");
        $etatCloture->setLabel("Clôturé");
        $etatEnCours->setLabel("En Cours");
        $etatTermine->setLabel("Terminé");
        $etatAnnule->setLabel("Annulé");
        $etatArchive->setLabel("Archivé");

        $manager->persist($etatEnCreation);
        $manager->persist($etatOuvert);
        $manager->persist($etatCloture);
        $manager->persist($etatEnCours);
        $manager->persist($etatTermine);
        $manager->persist($etatAnnule);
        $manager->persist($etatArchive);

        $manager->flush();
    }

    public function addSite(ObjectManager $manager, Generator $generator) {
        for ($i = 0; $i < 5; $i++) {
            $site = new Site();
            $site->setName($generator->city);

            $manager->persist($site);
        }
        $manager->flush();
    }

    public function addCity(ObjectManager $manager, Generator $generator) {
        for ($i = 0; $i < 5; $i++) {
            $ville = new City();
            $ville->setName($generator->city);
            $ville->setZipCode("99999");

            $manager->persist($ville);
        }
        $manager->flush();
    }

    public function addPlace(ObjectManager $manager, Generator $generator) {
        $villes = $manager->getRepository(City::class)->findAll();
        for ($i = 0; $i < 5; $i++) {
            $lieu = new Place();
            $lieu->setName($generator->word);
            $lieu->setStreet($generator->streetAddress);
            $lieu->setLatitude($generator->latitude);
            $lieu->setLongitude($generator->longitude);
            $lieu->setCity($generator->randomElement($villes));

            $manager->persist($lieu);
        }
        $manager->flush();
    }

    public function addEvent(ObjectManager $manager, Generator $generator) {
        $etats = $manager->getRepository(State::class)->findAll();
        $sites = $manager->getRepository(Site::class)->findAll();
        $lieux = $manager->getRepository(Place::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 15; $i++) {
            $sortie = new Event();
            $sortie->setName($generator->word);
            $sortie->setCampus($generator->randomElement($sites));
            $sortie->setPlace($generator->randomElement($lieux));
            $sortie->setState($generator->randomElement($etats));
            $sortie->setStartingTime($generator->dateTimeThisYear);
            $date = clone $sortie->getStartingTime();
            //refaire avec le clone
            $sortie->setDuration($generator->numberBetween(2,12));
            $sortie->setLastRegistrationDate($date->sub(new DateInterval('P1D')));
            $sortie->setNbMaxRegistration($generator->numberBetween(6,12));
            $sortie->setInformation($generator->word);
            $sortie->setOrganisateur($generator->randomElement($users));
            $sortie->addLstUser($generator->randomElement($users));

            $manager->persist($sortie);
        }
        $manager->flush();
    }

    public function addUser(ObjectManager $manager, Generator $generator) {
        $sites = $manager->getRepository(Site::class)->findAll();
        $roles = ['ROLE_USER', 'ROLE_ADMIN'];
        for ($i = 0; $i < 5; $i++ ) {
            $user = new User();
            $user->setLastname($generator->lastName);
            $user->setFirstname($generator->firstName);
            $user->setEmail($generator->email);
            $user->setIsActive($generator->boolean());
            $user->setIsAdmin($generator->boolean());
            $user->setRoles(['ROLE_ADMIN']);
            $user->setCampus($generator->randomElement($sites));
            $user->setPhoneNumber("0299064578");

            $plainPassword = "123456";

            $encoded = $this->encoder->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);

            $manager->persist($user);
        }
        $administrator = new User();
        $administrator->setEmail($generator->email);
        $administrator->setIsAdmin(true);
        $administrator->setRoles(['ROLE_ADMIN']);
        $administrator->setIsActive(true);
        $administrator->setFirstname($generator->lastName);
        $administrator->setLastname($generator->firstName);
        $administrator->setPhoneNumber("0512568745");
        $administrator->setCampus($generator->randomElement($sites));

        $plainPassword = "admin";
        $encoded = $this->encoder->encodePassword($administrator, $plainPassword);
        $administrator->setPassword($encoded);
        $manager->persist($administrator);



        $manager->flush();
    }
}